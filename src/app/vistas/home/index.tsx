import * as React from 'react';
import { Component } from 'react';

import style from './style.scss';
import { Hero, DobleHome } from './../../componentes/partes';

interface Props {

}
interface State {
}

import fondo from '../../assets/home/fondo.png';
import doble from '../../assets/home/doble.png';
import QueHacemos from '../../componentes/quehacemos';
import Blog from '../../componentes/blog';


export default function Home(props:any){
    return (
        <div className={style.wrapper} id="home">
            <Hero imagen={fondo} />
            <DobleHome imagen={doble} />
            <QueHacemos />
            <Blog />
        </div>
    );
}