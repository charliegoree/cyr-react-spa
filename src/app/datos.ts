import cocina1 from './assets/pagina/cocinas/bergamo.png';
import cocina2 from './assets/pagina/cocinas/napoles.png';
import cocina3 from './assets/pagina/cocinas/turin.png';
import cocina4 from './assets/pagina/cocinas/venecia.png';

import dormitorio1 from './assets/pagina/dormitorios/dormitorios.png';
import dormitorio2 from './assets/pagina/dormitorios/juveniles.png';

import vestidores1 from './assets/pagina/vestidores/florencia.png';
import vestidores2 from './assets/pagina/vestidores/genova.png';
import vestidores3 from './assets/pagina/vestidores/roma.png';

import juveniles1 from './assets/lineas/dormitorios/Juveniles/imagen1.png';
import juveniles2 from './assets/lineas/dormitorios/Juveniles/imagen2.png';
import juveniles3 from './assets/lineas/dormitorios/Juveniles/imagen3.png';
import juveniles4 from './assets/lineas/dormitorios/Juveniles/imagen4.png';

import dormitorios1 from './assets/lineas/dormitorios/Dormitorios/imagen1.png';
import dormitorios2 from './assets/lineas/dormitorios/Dormitorios/imagen2.png';
import dormitorios3 from './assets/lineas/dormitorios/Dormitorios/imagen3.png';
import dormitorios4 from './assets/lineas/dormitorios/Dormitorios/imagen4.png';

import banos1 from './assets/lineas/baños/imagen1.png';
import banos2 from './assets/lineas/baños/imagen2.png';
import banos3 from './assets/lineas/baños/imagen3.png';
import banos4 from './assets/lineas/baños/imagen4.png';

import muebles1 from './assets/lineas/muebles/imagen1.png';
import muebles2 from './assets/lineas/muebles/imagen2.png';
import muebles3 from './assets/lineas/muebles/imagen3.png';
import muebles4 from './assets/lineas/muebles/imagen4.png';

import bergamo1 from './assets/lineas/cocinas/cocina bergamo/imagen1.png';
import bergamo2 from './assets/lineas/cocinas/cocina bergamo/imagen2.png';
import bergamo3 from './assets/lineas/cocinas/cocina bergamo/imagen3.png';
import bergamo4 from './assets/lineas/cocinas/cocina bergamo/imagen4.png';

import napoles1 from './assets/lineas/cocinas/cocina napoles/imagen1.png';
import napoles2 from './assets/lineas/cocinas/cocina napoles/imagen2.png';
import napoles3 from './assets/lineas/cocinas/cocina napoles/imagen3.png';
import napoles4 from './assets/lineas/cocinas/cocina napoles/imagen4.png';

import turin1 from './assets/lineas/cocinas/cocina turin/imagen1.png';
import turin2 from './assets/lineas/cocinas/cocina turin/imagen2.png';
import turin3 from './assets/lineas/cocinas/cocina turin/imagen3.png';
import turin4 from './assets/lineas/cocinas/cocina turin/imagen4.png';

import venecia1 from './assets/lineas/cocinas/cocina venecia/imagen1.png';
import venecia2 from './assets/lineas/cocinas/cocina venecia/imagen2.png';
import venecia3 from './assets/lineas/cocinas/cocina venecia/imagen3.png';
import venecia4 from './assets/lineas/cocinas/cocina venecia/imagen4.png';

import florencia1 from './assets/lineas/vestidores/florencia/imagen1.png';
import florencia2 from './assets/lineas/vestidores/florencia/imagen2.png';
import florencia3 from './assets/lineas/vestidores/florencia/imagen3.png';
import florencia4 from './assets/lineas/vestidores/florencia/imagen4.png';

import genova1 from './assets/lineas/vestidores/genova/imagen1.png';
import genova2 from './assets/lineas/vestidores/genova/imagen2.png';
import genova3 from './assets/lineas/vestidores/genova/imagen3.png';
import genova4 from './assets/lineas/vestidores/genova/imagen4.png';

import roma1 from './assets/lineas/vestidores/roma/imagen1.png';
import roma2 from './assets/lineas/vestidores/roma/imagen2.png';
import roma3 from './assets/lineas/vestidores/roma/imagen3.png';
import roma4 from './assets/lineas/vestidores/roma/imagen4.png';


export const lineas = {
    cocinas: [
        {titulo:'Bergamo', imagen:cocina1, url:"/cocinas/bergamo" },
        {titulo:'Napoles', imagen:cocina2, url:"/cocinas/napoles" },
        {titulo:'Turin', imagen:cocina3, url:"/cocinas/turin" },
        {titulo:'Venecia', imagen:cocina4, url:"/cocinas/venecia" }
    ],
    dormitorios: [
        {titulo:'Dormitorio', imagen:dormitorio1, url:"/dormitorios/dormitorio" },
        {titulo:'Juveniles', imagen:dormitorio2, url:"/dormitorios/juveniles" },
    ],
    vestidores: [
        {titulo:'Florencia', imagen:vestidores1, url:"/vestidores/florencia" },
        {titulo:'Genova', imagen:vestidores2, url:"/vestidores/genova" },
        {titulo:'Roma', imagen:vestidores3, url:"/vestidores/roma" },
    ]
}

export const paginas = {
    dormitorios: {
        juveniles: {
            titulo: 'Juveniles',
            textoFondo1: 'DORMITORIOS',
            textoFondo2: 'Dormitorios',
            texto: 'Diseñamos y desarrollamos de manera integral dormitorios para niños y adolecentes divertidos y super funcionales para que puedan seguir adáptandose en las distintas etapas de crecimiento.',
            imagen1: juveniles1,
            imagen2: juveniles2,
            tituloConoce: 'Dormitorios Juveniles',
            texto1Conoce: 'Nuestro trabajo destaca por aunar funcionalidad y estética, siempre de forma personalizada, aportando soluciones a medida para todo tipo de espacios.',
            texto2Conoce: 'Diseñamos y fabricamos amoblamientos a medida, dependiendo de cuáles sean tus necesidades, pondremos a tu disposición.',
            imagen1Conoce: juveniles3,
            imagen2Conoce: juveniles4
        },
        dormitorios: {
            titulo: 'Dormitorios',
            textoFondo1: 'DORMITORIOS',
            textoFondo2: 'Dormitorios',
            texto: 'Diseños vanguardistas, frescos, jugando con colores, formas y materiales, lacas, vidrio, enchapado y ciernás.',
            imagen1: dormitorios1,
            imagen2: dormitorios2,
            tituloConoce: 'Todo para tu hogar',
            texto1Conoce: 'Puertas y frentes realizados en perfilería de aluminio y cristal pintado con laca y poliuretánica de alta adhesión.',
            texto2Conoce: 'Buscamos diseñar amoblamientos de primera calidad, y sobre todo, confortables, de forma personalizada, aportando soluciones a medida para los diferentes hábitos o rutinas de vida.',
            imagen1Conoce: dormitorios3,
            imagen2Conoce: dormitorios4
        }, 
    },
    vestidores: {
        florencia:{
            titulo: 'Florencia',
            textoFondo1: 'PLACARD',
            textoFondo2: 'Florencia',
            texto: 'El Placard Florencia ofrece frentes lisos integrado con un nuevo sistema corredizo donde las guias no se encuentran a la vista ayudando a mantener ocultos los sectores de guardado de indumentaria, calzado y accesorios.',
            imagen1: florencia1,
            imagen2: florencia2,
            tituloConoce: 'Placard Corredizo',
            texto1Conoce: 'Este placard se realiza en melanina con cantos de PVC a tono.',
            texto2Conoce: 'Los frentes pueden ser en vidrio, en espejos o en el enchapado deseado. Las divisiones interiores varían según la necesidad de cada cliente para mantener el espacio organizado y funcional.',
            imagen1Conoce: florencia3,
            imagen2Conoce: florencia4
        },
        roma:{
            titulo: 'Roma',
            textoFondo1: 'PLACARD',
            textoFondo2: 'Roma',
            texto: 'El placard Roma ofrece frentes con perfiles de aluminio a la vista, detalles unicos donde las guías no se encuentran a la vista ayudando a mantener ocultos los sectores de guardado de indumentaria, calzado y accesorios.',
            imagen1: roma1,
            imagen2: roma2,
            tituloConoce: 'Placard Corredizo',
            texto1Conoce: 'Este placard se realiza en melanina con cantos de PVC a tono.',
            texto2Conoce: 'Los frentes pueden ser en vidrio, en espejos o en el enchapado deseado. Las divisiones interiores varían según la necesidad de cada cliente para mantener el espacio organizado y funcional.',
            imagen1Conoce: roma3,
            imagen2Conoce: roma4
        },
        genova:{
            titulo: 'Genova',
            textoFondo1: 'PLACARD',
            textoFondo2: 'Genova',
            texto: 'El vestidor Génova ofrece un gran espacio de guardado y una máxima funcionalidad. Todo esta ordenado, a la vista y de fácil acceso, para hacer tu vida mas fácil y rápida.',
            imagen1: genova1,
            imagen2: genova2,
            tituloConoce: 'Combina con vos',
            texto1Conoce: 'El vestidor Génova es una opción para los amantes de la ropa y el orden. Estantes, colgantes y cajones continuos dan como resultado un mueble despojado de tiradores y máxima funcionalidad.',
            texto2Conoce: 'Recomendado para ambientes amplios, normalmente se lo arma sin puertas, facilitando la visión y la búsqueda de tus objetos, combinado con la ambientacíón del espacio. Vestidor Génova, combina con vos.',
            imagen1Conoce: genova3,
            imagen2Conoce: genova4
        }
    },
    cocinas:{
        turin: {
            titulo: 'Turin',
            textoFondo1: 'COCINA',
            textoFondo2: 'Turin',
            texto: 'Una cocina de diseño italiano de aspecto intenso y brillante. invita a imaginar una cocina de última generación, creando un ambiente cálido y atractivo.',
            imagen1: turin1,
            imagen2: turin2,
            tituloConoce: 'Superficies con laminado de brillo intenso',
            texto1Conoce: 'RAUVISIO es el laminado de brillo intenso de última generación.',
            texto2Conoce: 'Gracias a su excelente efecto relieve brillante puede sustituir los costosos elementos lacados de alta calidad. El resultado? Un brillo perfecto en tu Cocina.',
            imagen1Conoce: turin3,
            imagen2Conoce: turin4
        },
        napoles: {
            titulo: 'Napoles',
            textoFondo1: 'COCINA',
            textoFondo2: 'Napoles',
            texto: 'Una cocina de diseño italiano de aspecto descontracturado y simple, invita a imaginar una cocina de diseño rapida, resistente y sobre todo, económica.',
            imagen1: napoles1,
            imagen2: napoles2,
            tituloConoce: 'Opción económica y rápida',
            texto1Conoce: 'El material que utilizamos para desarrollar esta cocina es melamina',
            texto2Conoce: 'Un producto con la mejor variedad de diseños en maderas y colores. Si estás buscando una cocina resistente y sin trabajo adicional de terminación, la cocina Nápoles es tu mejor opción.',
            imagen1Conoce: napoles3,
            imagen2Conoce: napoles4
        },
        venecia: {
            titulo: 'Venecia',
            textoFondo1: 'COCINA',
            textoFondo2: 'Venecia',
            texto: 'Una cocina de diseño italiano de aspecto sofisticado, inspirado en el nuevo concepto cristal, como fuerte impulso de innovación. Su belleza, su variedad de colores y su posibilidad de personalización, la distingue del resto.',
            imagen1: venecia1,
            imagen2: venecia2,
            tituloConoce: 'Solo para exigentes',
            texto1Conoce: 'Puertas y frentes realizados en perfilería en aluminio y cristal pintado con laca polluretánica de alta adhesión.',
            texto2Conoce: 'Este material le da la posibilidad de elegir su color en un catálogo de 700 colores!. Puede combinar colores para darle a tu cocina un toque diferente. Si estás buscando una solución fina y elegante, la Cocina Venecia es tu mejor elección.',
            imagen1Conoce: venecia3,
            imagen2Conoce: venecia4
        },
        bergamo: {
            titulo: 'Bergamo',
            textoFondo1: 'COCINA',
            textoFondo2: 'Bergamo',
            texto: 'Una cocina de diseño italiano de aspecto elegante. que alucina e invita a imaginar una cocina soñada, creando un ambiente alegre y sumamente moderno.',
            imagen1: bergamo1,
            imagen2: bergamo2,
            tituloConoce: 'Moderna y Duradera',
            texto1Conoce: 'Con terminaciones únicas, este revestimiento ofrece versatilidad para la elaboración de diversos diseños.',
            texto2Conoce: 'Con colores inalterables, fácil mantenimiento, resistencia de la humedad, impacto y abrasión. Si esta buscando una solución a largo plazo, la Cocina Bérgamo es tu mejor elección.',
            imagen1Conoce: bergamo3,
            imagen2Conoce: bergamo4
        }
    },
    muebles:{
        titulo: 'Muebles',
        textoFondo1: 'MUEBLES',
        textoFondo2: 'Hogar',
        texto: 'El espacio que vivis todos los dias merece el mayor diseño. Pensamos, Diseñamos y Fabricamos todo tipo de muebles para hacer tu vida mas confortable y práctica.',
        imagen1: muebles1,
        imagen2: muebles2,
        tituloConoce: 'Tu lugar donde todo es posible',
        texto1Conoce: 'Trabajamos para crear espacios de calidad perdurables en el tiempo.',
        texto2Conoce: 'Diseñamos y fabricamos amoblamientos a medida, dependiendo de cuales sean tus necesidades, pondremos a tu disposición multitud de diseños y gran variedad de materiales, Envianos tu idea y recibí asesoramiento por parte de nuestros profesionales.',
        imagen1Conoce: muebles3,
        imagen2Conoce: muebles4
    },
    banos: {
        titulo: 'Baños',
        textoFondo1: 'REFORMA',
        textoFondo2: 'Baños',
        texto: 'Rediseñamos baños y ofrecemos soluciones innovadoras que mejoraran tu calidad de vida. hacemos todo tipo de reformas, integrales y parciales de baños, adaptandonos al espacio y estilo de cada persona.',
        imagen1: banos1,
        imagen2: banos2,
        tituloConoce: 'Reforma tu baño',
        texto1Conoce: 'Dependiendo de cuales sean tus necesidades, pondremos a tu disposición multitud de diseños y gran variedad de materiales.',
        texto2Conoce: 'Contamos con amplia variedad de bachas y grifería de primera calidad con modernos diseños para que tu baño se destaque. Nuestros proyectos aseguran un diseño innovador y un óptimo aprovechamiento del espacio otorgando la comodidad e intimidad que precisa.',
        imagen1Conoce: banos3,
        imagen2Conoce: banos4
    }
}

