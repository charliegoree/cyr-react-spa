import { Component, useState } from 'react';
import * as React from 'react';
import * as emailjs from "emailjs-com";
import style from './style.scss';

export default function Contacto(props: any){
    const [nombre,setNombre] = useState<string>('');
    const [apellido,setApellido] = useState<string>('');
    const [email,setEmail] = useState<string>('');
    const [mensaje,setMensaje] = useState<string>('');
    function enviarMail(){
		emailjs.send('mailjet', 'cyr', {nombre:nombre,apellido:apellido,email:email,mensaje:mensaje})
	    .then(function(response:any) {
	       console.log(response);
	    }, function(error:any) {
	    	console.log(error);
	    });
    }
    return(
        <div className={style.contenedor} id="contacto">
            <h1>¿TENÉS PENSADO REFORMAR TU CASA?</h1>
            <p>¡ENVÍANOS TU CONSULTA!</p>
            <form method="post">
                <div className={style.fila}>
                    <input type="text" name="nombre" placeholder="Nombre" required value={nombre} onChange={(e)=> setNombre(e.currentTarget.value)} />
                    <input type="text" name="apellido" placeholder="Apellido" required value={apellido} onChange={(e)=> setApellido(e.currentTarget.value)} />
                </div>
                <div className={style.fila}>
                    <input type="email" name="mail" placeholder="email" required value={email} onChange={(e)=> setEmail(e.currentTarget.value)} />
                </div>
                <div className={style.fila} id="area">
                    <textarea name="mensaje" placeholder="Escribe tu mensaje" required value={mensaje} onChange={(e)=> setMensaje(e.currentTarget.value)}></textarea>
                </div>
                <div className={style.fila}>
                    <button onMouseDown={(e)=>enviarmail()}>Enviar Mensaje</button>
                </div>
            </form>
        </div>
    );
}
